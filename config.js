exports.values = {
    dbUrl: 'http://localhost:8529',
    dbName: 'yiataCache',
    dbUserName: 'root',
    dbPassword: 'root',
    staticFilesBaseAddress : './HotelStaticData/',
    ratesFileAvailable: true,
    ratesFileAddress: './rates.json'
};